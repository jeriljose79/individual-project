import pygame
import random

pygame.font.init()

arial = pygame.font.SysFont("fonts/clean_numbers.otf", 40)
lemon_milk = pygame.font.SysFont("arial", 19)
clean_font_large = pygame.font.SysFont("fonts/lmf.otf", 100)
clean_font = pygame.font.SysFont("fonts/lmf.otf", 50)
clean_font_med = pygame.font.SysFont("fonts/lmf.otf", 35)
clean_font_small = pygame.font.SysFont("fonts/lmf.otf", 25)

WIDTH, HEIGHT = 1200, 900
MAX_ROWS = 13
MIN_ROWS = 3
TILE_SIZE = HEIGHT / (12 * 1.22)
DICE_SIZE = 100

board_layout = []

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)

WHITE_SMOKE = (255, 255, 255)
grey = (150, 150, 150)
CLEAN_BLACK = (24, 24, 24)
CAPTURED_COL = (70, 70, 70)

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("The can't stop Game")




class Board:
    global board_layout
    def __init__(self):
        self.selected_piece = None
        self.white_on = 0
        self.black_on = 0
        self.selected_tile = None
        self.board = []
        self.blocked_cols = []

    #creating board from list
    def create_board(self):
        counter = 0
        for col in board_layout:
            self.board.append([])
            for s in col:
                self.board[counter].append(0)
            
            counter += 1

    #loading board from 'board.txt' file, there it is defined with a grid layout
    def load_board(self):
        global board_layout
        with open("board.txt") as file:
            for line in file:
                tiles = []
                for i in range(0, len(line), 2):
                    tiles.append(line[i])
                board_layout.append(tiles)

    #drawing every tile and piece on the board
    def draw(self):
        counter = 2
        x_pos = 0.2
        for line in board_layout:
            y_pos = 0.4
            for tile in line:
                #checking what type of piece to draw
                if tile == "F":
                    if counter - 2 in self.blocked_cols:
                        pygame.draw.rect(WIN, CAPTURED_COL, (TILE_SIZE * x_pos, TILE_SIZE * y_pos, TILE_SIZE, TILE_SIZE), width=2)
                    else:
                        pygame.draw.rect(WIN, CLEAN_BLACK, (TILE_SIZE * x_pos, TILE_SIZE * y_pos, TILE_SIZE, TILE_SIZE), width=2)
                elif tile == "Z":
                    if counter - 2 in self.blocked_cols:
                        pygame.draw.rect(WIN, CAPTURED_COL, (TILE_SIZE * x_pos, TILE_SIZE * y_pos, TILE_SIZE, TILE_SIZE))
                    else:
                        pygame.draw.rect(WIN, CLEAN_BLACK, (TILE_SIZE * x_pos, TILE_SIZE * y_pos, TILE_SIZE, TILE_SIZE))
                    number = arial.render(str(counter), False, WHITE)
                    WIN.blit(number, (TILE_SIZE * x_pos + TILE_SIZE / 2 - number.get_width() / 2, TILE_SIZE * y_pos + TILE_SIZE / 2 - number.get_height() / 2))
            
                y_pos += 1.1
            x_pos += 1.2
            counter += 1

        #drawing placed pieces
        for col in self.board:
            for row in col:
                if row != 0 and type(row) != list:
                    row.draw()
                elif type(row) == list:
                    row[0].draw()
                    row[1].draw()


#piece class
class Piece:
    def __init__(self, row, col, color, piece_after=False):
        self.row = row
        self.col = col
        self.color = color
        self.piece_after = piece_after

        self.calc_pos(row, col)

    #class I didnt use
    def draw(self):
        if self.piece_after:
            rect = pygame.Rect(self.x, self.y, TILE_SIZE // 4, TILE_SIZE // 2)
        else:
            rect = pygame.Rect(self.x, self.y, TILE_SIZE // 2, TILE_SIZE // 2)
        pygame.draw.rect(WIN, self.color, rect)
    
    #moving the piece up when rolling a dice (so only the row the piece is in changes)
    def move(self, row, col):
        self.row = row
        self.col = col
        #calculation new position of piece
        self.calc_pos()

    #calculating piece position
    def calc_pos(self, row, col):
        #same as for the map drawing, so the piece can be drawn at same coordinates
        x_pos, y_pos = 0.2, 0.4
        for i in range(row):
            y_pos += 1.1
        for i in range(col):
            x_pos += 1.2
        self.x = TILE_SIZE * x_pos + TILE_SIZE // 4
        self.y = TILE_SIZE * y_pos + TILE_SIZE // 4

#this is the game class where every essential game mechanics are defined
class Game:
    def __init__(self):
        #defining a WHOLE LOT of variables that I use in the different functions, most of them speak for themselfs
        self.selected = None
        self.board = Board()
        self.turn = RED
        self.dice = []
        self.neutral_pieces = {}
        self.blue_sum = 0
        self.red_sum = 0
        self.roll_button = False
        self.can_roll = True
        self.can_move = True
        self.can_stop = False
        self.in_last_col = {}
        self.used_placeholders = 0
        self.passed_black = 0
        self.passed_white = 0
        self.round_progress = {}
        self.total_progress_black = {}
        
        #defining the drawing variables in the game class for better performance
        self.rect = pygame.rect.Rect((0, 0, WIDTH, 20))
        self.number = clean_font.render("2", False, WHITE)
        self.choice = clean_font.render("Dice Choice:", False, CLEAN_BLACK)
        self.stop = clean_font_med.render("STOP", False, CLEAN_BLACK)
        self.roll = clean_font_med.render("ROLL", False, CLEAN_BLACK)
        self.roll_rect = pygame.rect.Rect(TILE_SIZE * (12 * 1.25), 540, self.choice.get_width(), 40)
        self.stop_rect = pygame.rect.Rect(TILE_SIZE * (12 * 1.25), 600, self.choice.get_width(), 40)
        self.move = clean_font_med.render("MOVE", False, CLEAN_BLACK)
        self.move_rect = pygame.rect.Rect(TILE_SIZE * (12 * 1.25), 660, self.choice.get_width(), 40)
        self.dice_rects = {
            (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.25 - 10, 380, self.number.get_width(), self.number.get_height()): (0, 0, 255),
            (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.75 - 10, 380, self.number.get_width(), self.number.get_height()): (0, 0, 255),
            (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.25 - 10, 460, self.number.get_width(), self.number.get_height()): (255, 0, 0),       
            (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.75 - 10, 460, self.number.get_width(), self.number.get_height()): (255, 0, 0)
        }


    #rolling the dice and returning the 4 rolled dice
    def roll_dice(self):
        self.dice.clear()
        self.can_roll = False
        self.can_move = True
        self.blue_sum = 0
        self.red_sum = 0
        for i in range(4):
            number = random.randint(1, 6)
            self.dice.append(number)
            if i <= 1:
                self.blue_sum += number
            else:
                self.red_sum += number
        counter = 0
        for k in self.dice_rects.keys():
            if counter <= 1:
                self.dice_rects[k] = BLUE
            else:
                self.dice_rects[k] = RED
            counter += 1

    #if someone has three passed pieces, they win
    def winner(self):
        if self.passed_white >= 3:
            return RED
        elif self.passed_black >= 3:
            return BLUE
        
        return None

    #get all possible dice combinations
    def get_all_pairs(self):
        possible_dice_pairs = []
        possible_dice_pairs.append([self.dice[0], self.dice[1]])
        possible_dice_pairs.append([self.dice[0], self.dice[2]])
        possible_dice_pairs.append([self.dice[0], self.dice[3]])
        possible_dice_pairs.append([self.dice[1], self.dice[2]])
        possible_dice_pairs.append([self.dice[1], self.dice[3]])
        possible_dice_pairs.append([self.dice[2], self.dice[3]])

        return possible_dice_pairs

    #check if piece is in last ROW
    def check_last_col(self):
        counter = 0
        if self.turn == BLUE:
            for col in self.board.board:
                if col[0] != 0:
                    if col[0].color == WHITE_SMOKE:
                        self.in_last_col[counter] = BLUE
                counter += 1

    #draw the buttons and the dice choices etc.
    def draw(self):
        mx, my = pygame.mouse.get_pos()

        if self.turn == RED:
            turn = lemon_milk.render("Player's Turn", False, WHITE)
            pygame.draw.rect(WIN, RED, self.rect)
        else:
            turn = lemon_milk.render("Computer's Turn", False, WHITE)
            pygame.draw.rect(WIN, BLUE, self.rect)

        if self.stop_rect.collidepoint((mx, my)):
            pygame.draw.rect(WIN, WHITE, self.stop_rect)
        pygame.draw.rect(WIN, CLEAN_BLACK, self.stop_rect, width=4)
        WIN.blit(self.stop, (self.stop_rect.x + self.stop_rect.width / 2 - self.stop.get_width() / 2, self.stop_rect.y + self.stop_rect.height / 2 - self.stop.get_height() / 2))

        if self.roll_rect.collidepoint((mx, my)):
            pygame.draw.rect(WIN, WHITE, self.roll_rect)
        pygame.draw.rect(WIN, CLEAN_BLACK, self.roll_rect, width=4)
        WIN.blit(self.roll, (self.roll_rect.x + self.roll_rect.width / 2 - self.roll.get_width() / 2, self.roll_rect.y + self.roll_rect.height / 2 - self.roll.get_height() / 2))

        if self.move_rect.collidepoint((mx, my)):
                pygame.draw.rect(WIN, WHITE, self.move_rect)
        pygame.draw.rect(WIN, CLEAN_BLACK, self.move_rect, width=4)
        WIN.blit(self.move, (self.move_rect.x + self.move_rect.width / 2 - self.move.get_width() / 2, self.move_rect.y + self.move_rect.height / 2 - self.move.get_height() / 2))

        WIN.blit(turn, (self.rect.x + WIDTH / 2 - turn.get_width() / 2, self.rect.y + self.rect.height / 2 - turn.get_height() / 2))

        if len(self.dice) == 4:
            WIN.blit(clean_font.render(str(self.dice[0]), False, BLACK), (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.25 - 10, 380))
            WIN.blit(clean_font.render(str(self.dice[1]), False, BLACK), (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.75 - 10, 380))
            WIN.blit(clean_font.render(str(self.dice[2]), False, BLACK), (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.25 - 10, 460))
            WIN.blit(clean_font.render(str(self.dice[3]), False, BLACK), (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.75 - 10, 460))
                
            WIN.blit(clean_font_med.render("BLUE SUM: " + str(self.blue_sum), False, BLACK), (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.25 - 20, 280))
            WIN.blit(clean_font_med.render("RED SUM: " + str(self.red_sum), False, BLACK), (TILE_SIZE * (12 * 1.25) + self.choice.get_width() * 0.25 - 20, 320))

            for rect in self.dice_rects.keys():
                pygame.draw.rect(WIN, self.dice_rects[rect], (rect[0] - 10, rect[1] - 10, rect[2] + 20, rect[3] + 20), 4)

    #moving, placing, calculating, etc. of placeholders: the most important function
    def place_holders(self):
        self.can_move = False
        self.can_roll = True
        self.can_stop = True
        #when placing place with both sums
        if self.used_placeholders < 3:
            sums = [self.blue_sum, self.red_sum]
        else:
            sums = [self.blue_sum]

        #getting the opposite color
        if self.turn == RED:
            opposite_color = BLUE
        else:
            opposite_color = RED
        #going through sums
        for sum in sums:
            pieces_found = False
            #col is always 2 more because we start at two, in the array we start at 0
            col = sum - 2
            if col not in self.board.blocked_cols:
                counter = 0
                for piece in self.board.board[col]:
                    #check if piece is from you
                    if piece != 0 and type(piece) != list:
                        if piece.color != opposite_color:
                            #piece of color found
                            pieces_found = True
                        if piece.color == WHITE_SMOKE:
                            #if piece is placeholder and noone is in the last col
                            if counter != 0:
                                #move the piece
                                if self.board.board[col][counter - 1] == 0:
                                    self.board.board[col][counter] = 0
                                    self.board.board[col][counter - 1] = Piece(counter - 1, col, WHITE_SMOKE)
                                    
                                else:
                                    self.board.board[col][counter] = 0
                                    self.board.board[col][counter - 1] = [self.board.board[col][counter - 1], Piece(counter - 1, col, WHITE_SMOKE, True)]
                                    self.can_stop = False
                                    
                                #adding progress
                                try:
                                    self.round_progress[col] += 1
                                except KeyError:
                                    self.round_progress[col] = 1

                                if self.turn == BLUE:
                                    try:
                                        self.total_progress_black[col] += 1
                                    except KeyError:
                                        self.total_progress_black[col] = 1
                                break


                            else:
                                #last row
                                self.in_last_col[self.turn] = col
                                self.change_turn(True)

                        #if there is a piece of your color   
                        elif piece.color != opposite_color:
                            if self.used_placeholders < 3:
                                if counter != 0:
                                    #place the piece
                                    if self.board.board[col][counter - 1] == 0:
                                        self.board.board[col][counter - 1] = Piece(counter - 1, col, WHITE_SMOKE)
                                        self.used_placeholders += 1
                                    else:
                                        self.board.board[col][counter - 1] = [self.board.board[col][counter - 1], Piece(counter - 1, col, WHITE_SMOKE, True)]
                                        self.can_stop = False
                                        self.used_placeholders += 1

                                    #same here
                                    try:
                                        self.round_progress[col] += 1
                                    except KeyError:
                                        self.round_progress[col] = 1


                                    if self.turn == BLUE:
                                        try:
                                            self.total_progress_black[col] += 1
                                        except KeyError:
                                            self.total_progress_black[col] = 1

                                #if piece is now in the last col, you append it to the list
                                else:
                                    self.in_last_col[self.turn] = col
                                    
                    #if there are two pieces merged
                    elif type(piece) == list:
                        pieces_found = True
                        #move the placeholder by one and leave the piece of opposite color there
                        self.board.board[col][counter] = piece[0]
                        self.board.board[col][counter - 1] = Piece(counter - 1, col, WHITE_SMOKE)

                        if self.turn == BLUE:
                            try:
                                self.total_progress_black[col] += 1
                            except KeyError:
                                self.total_progress_black[col] = 1
                            try:
                                self.round_progress[col] += 1
                            except KeyError:
                                self.round_progress[col] = 1

                        break


                    counter += 1
                #if no pieces in col found
                if not pieces_found:
                    #place new placeholder in col if there are less than 3 placeholders placed
                    if self.used_placeholders < 3:
                        if col <= 5:
                            #getting col and row and then placing your piece on the last of the row
                            self.board.board[col][12 - ((-col + 5) * 2)]
                            if self.board.board[col][12 - ((-col + 5) * 2)] == 0:
                                self.board.board[col][12 - ((-col + 5) * 2)] = Piece(12 - ((-col + 5) * 2), col, WHITE_SMOKE)
                                self.used_placeholders += 1
                            else:
                                self.board.board[col][12 - ((-col + 5) * 2)] = [self.board.board[col][12 - ((-col + 5) * 2)], Piece(12 - ((-col + 5) * 2), col, WHITE_SMOKE, True)]
                                self.used_placeholders += 1

                            self.round_progress[col] = 1
                            if self.turn == BLUE:
                                self.total_progress_black[col] = 1
                        elif col >= 6:
                            if self.board.board[col][10 - ((col - 6) * 2)] == 0:
                                self.board.board[col][10 - ((col - 6) * 2)] = Piece(10 - ((col - 6) * 2), col, WHITE_SMOKE)
                                self.used_placeholders += 1
                            else:
                                self.board.board[col][10 - ((col - 6) * 2)] = [self.board.board[col][10 - ((col - 6) * 2)], Piece(10 - ((col - 6) * 2), col, WHITE_SMOKE, True)]
                                self.used_placeholders += 1

                            self.round_progress[col] = 1
                            if self.turn == BLUE:
                                self.total_progress_black[col] = 1

    #changing turn
    def change_turn(self, cant_move): #check if the player could not move and turn is changed because of that
        col_counter = 0
        for col in self.board.board:
            counter = 0
            piece_counter = 15
            for piece in col:
                if piece != 0 and type(piece) != list:
                    #replacing every piece with color piece
                    if piece.color == WHITE_SMOKE:
                        #if player couldnt move then all placeholders are removed
                        if cant_move:
                            self.board.board[piece.col][piece.row] = 0
                        else:
                            piece_counter = counter
                            self.board.board[piece.col][piece.row] = Piece(piece.row, piece.col, self.turn)
                            if counter == 0:
                                if self.turn == BLUE:
                                    self.passed_black += 1
                                elif self.turn == RED:
                                    self.passed_white += 1
                                self.board.blocked_cols.append(col_counter)
                    else: 
                        if piece.color == self.turn and counter > piece_counter:
                            self.board.board[piece.col][piece.row] = 0

                elif type(piece) == list:
                    if cant_move:
                        self.board.board[piece[0].col][piece[0].row] = piece[0]
                    else:
                        self.can_stop = False
                        self.used_placeholders = 0
                        self.in_last_col = {}
                        self.round_progress = {}
                        return False
                counter += 1

            col_counter += 1
        #setting variables back for new turn
        self.used_placeholders = 0
        self.in_last_col = {}
        self.round_progress = {}
        self.can_roll = True
        self.can_move = False
        #changing turn
        if self.turn == RED:
            self.turn = BLUE
        else:
            self.turn = RED

    #check if there are pieces merged
    def check_merged(self):
        merged = []
        counter = 0
        for col in self.board.board:
            for row in col:
                if type(row) == list:
                    merged.append(counter)
            counter += 1
        return merged

    #check if player has no moves
    def check_no_moves(self, sum):
        if sum not in self.board.blocked_cols or self.board.board[sum][0] == 0:
            for piece in self.board.board[sum]:
                if piece != 0 and type(piece) != list:
                    if self.turn == BLUE:
                        if piece.color != RED:
                            if piece.color == BLUE and self.used_placeholders < 3 or piece.color == WHITE_SMOKE:
                                return False
                    elif self.turn == RED:
                        if piece.color != BLUE:
                            if piece.color == RED and self.used_placeholders < 3 or piece.color == WHITE_SMOKE:
                                return False

                if type(piece) == list:
                    return False
        return True


            

class AI:

    def __init__(self, game):
        self.game = game
    
    #decide whether or not to make another roll
    def decision(self):
        probabilities = {2: 0.13, 3: 0.23, 4: 0.36, 5: 0.45, 6: 0.56, 7: 0.64, 8: 0.56, 9: 0.45, 10: 0.36, 11: 0.23, 12: 0.13}
        probabilities2 = {2: 0.02778, 3: 0.05556, 4: 0.08333, 5: 0.11111, 6: 0.13889, 7: 0.16667, 8: 0.13889, 9: 0.11111, 10: 0.08333, 11: 0.05556, 12: 0.02778}

        #checking if placeholders are not all placed, if there is a placeholder in last col and if there are merged pieces
        if self.game.turn == BLUE:
            if self.game.check_merged() != []:
                return True
            for k in self.game.in_last_col.keys():
                if self.game.in_last_col[k] == BLUE:
                    return False
            if self.game.used_placeholders < 3:
                return True


        cols = []
        progress = []
        l = []
        w = []
        prob = 0

        #appending cols, progress, ...
        for col in self.game.round_progress.keys():
            cols.append(col + 2)
            progress.append(self.game.round_progress[col])
            l.append(len(self.game.board.board[col]))
            prob += probabilities[col + 2]

        s = 0
        risk = 0.8
        counter = 0
        #calculating weight of moves
        for col in cols:
            #calculation by: progress / len of column
            w1 = progress[counter]/l[counter]
            w.append(w1)
            counter += 1

        #based on probability and weight deciding to stop or not
        if sum(w) > 0.4 and prob < 0.4:
            return False
        if sum(w) < risk:
            return True
        return False

    #choosing a pair of dice
    def choose_dice(self):
        if self.game.turn == BLUE:
            rel_progress = {}
            options = []
            merged = self.game.check_merged()
            #for every possible dice pair
            for dice_pair in self.game.get_all_pairs():
                #if the dice pair can move
                if sum(dice_pair) - 2 not in self.game.board.blocked_cols and self.game.board.board[sum(dice_pair) - 2][0] == 0:
                    #if the dice pair is a merged piece: return it to move the merged out
                    if sum(dice_pair) - 2 in merged:
                        return dice_pair
                    #if dice pair in already moved placeholders
                    if sum(dice_pair) - 2 in self.game.round_progress.keys():
                        for k in self.game.round_progress.keys():
                            if dice_pair[0] + dice_pair[1] - 2 == k:
                                #append to relative_progress
                                rel_progress[round(self.game.total_progress_black[k] / len(self.game.board.board[k]), 2)] = sum(dice_pair)

                    #if not all placeholders are used
                    if self.game.used_placeholders < 3:
                        #append to rel_progress
                        if self.game.board.board[sum(dice_pair) - 2][-1] != 0 and type(self.game.board.board[sum(dice_pair) - 2][-1]) != list:
                            if self.game.board.board[sum(dice_pair) - 2][-1].color == RED:
                                rel_progress[0] = sum(dice_pair)
                            else:
                                rel_progress[round(1 / len(self.game.board.board[sum(dice_pair) - 2]), 2)] = sum(dice_pair)
                        else:
                            rel_progress[round(1 / len(self.game.board.board[sum(dice_pair) - 2]), 2)] = sum(dice_pair)

            for k in rel_progress.keys():
                options.append(k)
            if options != []:
                #getting the best option and returning it
                r = rel_progress[max(options)]
                for dice_pair in self.game.get_all_pairs():
                    if sum(dice_pair) == r:
                        return dice_pair

            #if nothing can be chosen then we return nothing
            return []

    def decide(self):
        #main funcion
        self.game.check_last_col()
        decision = self.decision()
        #decide
                  
        if decision == True:
            #roll dice
            self.game.roll_dice()
            chosen_pair = self.choose_dice()
            #get chosen pair
            #checking if ai can move or not, if not, turn is changed and progress lost
            if self.game.used_placeholders == 3 and len(chosen_pair) == 2:
                if self.game.check_no_moves(sum(chosen_pair) - 2):
                    self.game.change_turn(True)
                    return False
            elif len(chosen_pair) == 0 and self.game.used_placeholders == 3:
                self.game.change_turn(True)
                return False
                
            try:
                i1 = self.game.dice.index(chosen_pair[0])
                i2 = self.game.dice.index(chosen_pair[1])
                if i2 == i1:
                    copied = self.game.dice.copy()
                    copied.pop(i1)
                    i2 = copied.index(chosen_pair[1])
                    i2 += 1
            except TypeError and IndexError:
                self.game.change_turn(True)
                return False
            other_i = []
            for i in range(0, 4):
                if i == i1 or i == i2:
                    pass
                else:
                    other_i.append(self.game.dice[i])

            #getting blue and red sum for display and to move
            self.game.blue_sum = sum(chosen_pair)
            self.game.red_sum = sum(other_i)
            counter = 0
            #displaying the rects around the chosen pairs
            for k in self.game.dice_rects.keys():
                if counter in [i2, i1]:
                    self.game.dice_rects[k] = BLUE
                else:
                    self.game.dice_rects[k] = RED
                counter += 1
        
        else:
            self.game.change_turn(False)

    #ai places and moves placeholders
    def ai_move(self):
        self.game.place_holders()




#main function
def main():
    #defining core var
    clock = pygame.time.Clock()
    FPS = 60
    run = True
    game = Game()
    game.board.load_board()
    game.board.create_board()
    ai = AI(game)
    ai_counter = 0

    #redrawing window
    def redraw_window():
        WIN.fill(grey)
        game.board.draw()
        game.draw()
        pygame.display.update()

    while run:
        #main loop
        clock.tick(FPS)
        print(clock)

        redraw_window()
        #ai moves
        if game.turn == BLUE:
            if ai_counter == 60:
                ai.decide()
            ai_counter += 1
            if ai_counter == FPS * 2:
                ai.ai_move()
                ai_counter = 0
        
        #if winner
        if game.winner():
            pygame.time.delay(500)
            if game.winner() == RED:
              return restart_menu("YOU")
            else:
              return restart_menu("AI")
            

        #checking for input
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                #quitting
            if game.turn == RED:
                #only if its the players turn
                if event.type == pygame.MOUSEBUTTONDOWN:
                    mx, my = pygame.mouse.get_pos()
                    #if player presses on roll button
                    if game.roll_rect.collidepoint((mx, my)):
                        if game.can_roll:
                            game.roll_dice()
                            pygame.time.wait(200)
                    #if player presses of stop button
                    elif game.stop_rect.collidepoint((mx, my)):
                        if game.can_stop:
                            if game.check_merged() == []:
                                if game.check_no_moves(game.blue_sum - 2):
                                    game.change_turn(True)
                                game.change_turn(False)
                    #if player presses on move button
                    elif game.move_rect.collidepoint((mx, my)):
                        if game.can_move:
                            blue_count = 0
                            red_count = 0
                            #checking some stuff
                            if game.used_placeholders == 3 and game.check_no_moves(game.blue_sum - 2):
                                game.change_turn(True)
                                game.can_roll = True
                            else:
                                for dice in game.dice_rects.keys():
                                    print(dice)
                                    if game.dice_rects[dice] == BLUE:
                                        blue_count += 1
                                    else:
                                        red_count += 1
                                if blue_count == 2 and red_count == 2:
                                    game.place_holders()
                    counter = 0
                    #checking if player clicks on the dice rects, changing their color and blue, red sum
                    for dice_rect in game.dice_rects.keys():
                        if pygame.Rect(dice_rect).collidepoint((mx, my)) and len(game.dice) == 4:
                            if game.dice_rects[dice_rect] == RED:
                                game.dice_rects[dice_rect] = BLUE
                                game.red_sum -= game.dice[counter]
                                game.blue_sum += game.dice[counter]
                            else:
                                game.dice_rects[dice_rect] = RED
                                game.blue_sum -= game.dice[counter]
                                game.red_sum += game.dice[counter]

                        counter += 1

        

        
#restart menu
def restart_menu(won):
    global board_layout
    board_layout = []
    clock = pygame.time.Clock()
    FPS = 30
    run = True

    game_ended = clean_font_large.render("GAME ENDED: " + won + " WON", False, WHITE)
    play = clean_font_med.render("PLAY AGAIN", False, WHITE)
    leave = clean_font_med.render("LEAVE", False, WHITE)
    play_rect = pygame.Rect(WIDTH / 2 + 50, HEIGHT / 2 + 80, 200, 80)
    leave_rect = pygame.Rect(WIDTH / 2 - 250, HEIGHT / 2 + 80, 200, 80)

    while run:
        clock.tick(FPS)
        WIN.fill(BLACK)
        pygame.draw.rect(WIN, RED, leave_rect, 5)
        pygame.draw.rect(WIN, (0, 255, 0), play_rect, 5)

        WIN.blit(play, (play_rect.x + play_rect.width / 2 - play.get_width() / 2, play_rect.y + play_rect.height / 2 - play.get_height() / 2))
        WIN.blit(leave, (leave_rect.x + leave_rect.width / 2 - leave.get_width() / 2, leave_rect.y + leave_rect.height / 2 - leave.get_height() / 2))
        WIN.blit(game_ended, (WIDTH / 2 - game_ended.get_width() / 2, HEIGHT / 2 - game_ended.get_height() / 2))

        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                mx, my = pygame.mouse.get_pos()
                if play_rect.collidepoint((mx, my)):
                    main()
                elif leave_rect.collidepoint((mx, my)):
                    pygame.quit()
            if event.type == pygame.QUIT:
                pygame.quit()
        

main()

